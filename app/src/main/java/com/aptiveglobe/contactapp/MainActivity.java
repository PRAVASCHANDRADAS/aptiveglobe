package com.aptiveglobe.contactapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ArrayList<ContactsModel> selectUsers;
    ArrayList<SelectedContactModel> selectedUsers;
    private RecyclerView recyclerView;
    private RecyclerAdapter adapter;
    Cursor phones;
    private Button btnNext;
    Spinner viewSpinner;

    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.contacts_list);
        btnNext = findViewById(R.id.btnNext);
        viewSpinner = findViewById(R.id.viewSpinner);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager= new LinearLayoutManager(MainActivity.this, LinearLayoutManager. VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        selectUsers = new ArrayList<ContactsModel>();
        showContacts();

        viewSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(position == 0){
                    LinearLayoutManager layoutManager= new LinearLayoutManager(MainActivity.this, LinearLayoutManager. VERTICAL, false);
                    recyclerView.setLayoutManager(layoutManager);
                }else{
                    recyclerView.setLayoutManager(new GridLayoutManager(MainActivity.this, 2));
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (adapter.getSelected().size() > 0) {
                    String name="",mobile="";
                    selectedUsers=new ArrayList<>();
                    for (int i = 0; i < adapter.getSelected().size(); i++) {
                        name=adapter.getSelected().get(i).getName();
                        mobile=adapter.getSelected().get(i).getPhone();
                        SelectedContactModel selectedcont=new SelectedContactModel(name,mobile);
                        selectedUsers.add(selectedcont);
                        System.out.println("Selectedcontacts==="+name+" /n "+mobile);

                    }
                    Intent intent =new Intent(MainActivity.this,SelectedContactActivity.class);
                    intent.putExtra("SELECTED_CONTACTS", selectedUsers);
                    startActivity(intent);
                } else {
                    showToast("No Selection");
                }
            }
        });
    }
    private void showContacts() {
        // Check the SDK version and whether the permission is already granted or not.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        } else {
            // Android version is lesser than 6.0 or the permission is already granted.
            phones = getApplicationContext().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
            LoadContact loadContact = new LoadContact();
            loadContact.execute();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                showContacts();
            } else {
                Toast.makeText(this, "Until you grant the permission, we canot display the names", Toast.LENGTH_SHORT).show();
            }
        }
    }
    class LoadContact extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected Void doInBackground(Void... voids) {
            // Get Contact list from Phone
            if (phones != null) {
                Log.e("count", "" + phones.getCount());
                if (phones.getCount() == 0) {
                }
                while (phones.moveToNext()) {
                    Bitmap bit_thumb = null;
                    String id = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
                    String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                    ContactsModel selectUser = new ContactsModel();
                    selectUser.setName(name);
                    selectUser.setPhone(phoneNumber);
                    selectUsers.add(selectUser);
                }
            } else {
                Log.e("Cursor close 1", "----------------");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // sortContacts();
            int count=selectUsers.size();
            ArrayList<ContactsModel> removed=new ArrayList<>();
            ArrayList<ContactsModel> contacts=new ArrayList<>();
            for(int i=0;i<selectUsers.size();i++){
                ContactsModel inviteFriendsProjo = selectUsers.get(i);

                if(inviteFriendsProjo.getName().matches("\\d+(?:\\.\\d+)?")||inviteFriendsProjo.getName().trim().length()==0){
                    removed.add(inviteFriendsProjo);
                    Log.d("Removed Contact",new Gson().toJson(inviteFriendsProjo));
                }else{
                    contacts.add(inviteFriendsProjo);
                }
            }
            contacts.addAll(removed);
            selectUsers=contacts;
            adapter = new RecyclerAdapter(inflater, selectUsers);
            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            recyclerView.setAdapter(adapter);
        }
    }
    public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
        private LayoutInflater layoutInflater;
        public List<ContactsModel> cont;
        ContactsModel list;
        private ArrayList<ContactsModel> arraylist;
        boolean checked = false;
        private Context context;
        View vv;

        public RecyclerAdapter(LayoutInflater inflater, List<ContactsModel> items) {
            this.layoutInflater = inflater;
            this.cont = items;
            this.arraylist = new ArrayList<ContactsModel>();
            this.arraylist.addAll(cont);
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = layoutInflater.inflate(R.layout.contactlist_rowlayout, parent, false);
            ViewHolder viewHolder = new ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            holder.bind(cont.get(position));
        }
        @Override
        public int getItemCount() {
            return cont.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            public TextView title;
            public TextView phone;
            public LinearLayout contact_select_layout;
            private ImageView imageView;

            public ViewHolder(View itemView) {
                super(itemView);
                this.setIsRecyclable(false);
                title = itemView.findViewById(R.id.name);
                phone = itemView.findViewById(R.id.no);
                imageView = itemView.findViewById(R.id.imageView);
                contact_select_layout = itemView.findViewById(R.id.contact_select_layout);
            }
            void bind(final ContactsModel contact) {
                imageView.setVisibility(contact.isChecked() ? View.VISIBLE : View.GONE);
                title.setText(contact.getName());
                phone.setText(contact.getPhone());

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        contact.setChecked(!contact.isChecked());
                        imageView.setVisibility(contact.isChecked() ? View.VISIBLE : View.GONE);
                    }
                });
            }
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        public ArrayList<ContactsModel> getSelected() {
            ArrayList<ContactsModel> selected = new ArrayList<>();
            for (int i = 0; i < cont.size(); i++) {
                if (cont.get(i).isChecked()) {
                    selected.add(cont.get(i));
                }
            }
            return selected;
        }

    }
    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}

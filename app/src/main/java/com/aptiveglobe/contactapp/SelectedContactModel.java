package com.aptiveglobe.contactapp;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class SelectedContactModel implements Serializable {
    public String getName_() {
        return name_;
    }

    public void setName_(String name_) {
        this.name_ = name_;
    }

    public String getPhone_() {
        return phone_;
    }

    public void setPhone_(String phone_) {
        this.phone_ = phone_;
    }

    String name_;
    String phone_;
    public SelectedContactModel(String name_,String phone_){
        this.name_=name_;
        this.phone_=phone_;
    }

}

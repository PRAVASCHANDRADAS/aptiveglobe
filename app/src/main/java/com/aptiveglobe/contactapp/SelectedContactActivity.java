package com.aptiveglobe.contactapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class SelectedContactActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    SelectedContactModel selectedContactModel;
    private ArrayList<SelectedContactModel> updatedData;
    private RecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_contact);
        recyclerView = findViewById(R.id.contacts_list);
        layoutManager = new LinearLayoutManager(SelectedContactActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        updatedData=new ArrayList<>();
        updatedData = (ArrayList<SelectedContactModel>) getIntent().getSerializableExtra("SELECTED_CONTACTS");
        System.out.println("UpdatedData"+updatedData.toString());

        adapter=new RecyclerAdapter(SelectedContactActivity.this,updatedData);
        recyclerView.setAdapter(adapter);
    }
    public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
        private Context mContext;
        List<SelectedContactModel> list;
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.contactlist_rowlayout, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }
        public RecyclerAdapter(Context mContext, List<SelectedContactModel> list) {
            this.mContext = mContext;
            this.list = list;
        }
        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            SelectedContactModel model = list.get(position);
            final String name = model.getName_();
            final String phone = model.getPhone_();

            holder.title.setText(name);
            holder.phone.setText(phone);
        }
        @Override
        public int getItemCount() {
            return list.size();
        }
        public  class ViewHolder extends RecyclerView.ViewHolder {
            public TextView title;
            public TextView phone;
            public ViewHolder(View itemView) {
                super(itemView);
                title = itemView.findViewById(R.id.name);
                phone = itemView.findViewById(R.id.no);
            }

        }
    }
}
